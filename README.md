## Workshop 2

1. Move login, logout from App to Home Page
2. And footer with current year an stick to bottom
3. Add pipe from homework and use Pipe in Header Text Logo

## Workshop 3

1. Navigate from Header to Home and Posts
2. Add DropDown Menu with UserName and Logout Icon<br>
   **material icons:** https://fonts.google.com/icons?selected=Material+Icons<br>
   **angular material:** https://material.angular.io/components/menu/examples
3. Add Posts filter (Search)
4. Watch udemy Observables, Reactive forms, Router.
5. Hot vs Cold Observable https://youtu.be/oKqcL-iMITY

## Workshop 4

1. Create Login Page:
   - When app starts login page is shown.
   - User must provide name and email.
   - After form submit user is redirected to Posts Page.
   - Display provided user name in the dropdown menu.
   - Add validators **required** to both fields.
   - Add validator **min length 3 symbols** to username field.
   - Display all errors in the form.
   - Button submit is disabled if form is invalid.
     <img src="./readme-images/login.PNG" width="600">
2. Create new post https://jsonplaceholder.typicode.com/guide/

## Workshop 5

1. Make all pages Lazy Modules
2. Handle Errors in Post Page Template
3. Create 2 sub pages for posts page: (**all-posts-page** and **my-posts-page**)
   - "all-posts-page" - display all posts
   - "my-posts-page" - display related posts to user by **id** 'https://jsonplaceholder.typicode.com/posts?userId=1',
   <br>
     where userId = logged in user Id
   <br>
     <img src="./readme-images/posts-structure.JPG" width="300">
4. Add nav menu for navigation between **my posts** and **all posts**
   <img src="./readme-images/posts.gif" width="600">

---

<!-- Update Angular CLI and init new project

```sh
npm uninstall -g @angular/cli
npm install -g @angular/cli@latest
ng new my-app
```

Install Tailwind

```sh
npm install -D tailwindcss
npx tailwindcss init
``` -->
