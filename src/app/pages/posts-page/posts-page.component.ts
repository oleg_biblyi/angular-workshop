import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Post } from './models';
import { PostsStateService } from './services';

@Component({
  selector: 'app-posts-page',
  templateUrl: './posts-page.component.html',
  styleUrls: ['./posts-page.component.scss'],
})
export class PostsPageComponent implements OnInit, OnDestroy {
  sub!: Subscription;

  searchForm = this.fb.group({
    query: '',
  });

  constructor(private postsStateService: PostsStateService, private fb: FormBuilder) {}

  ngOnInit(): void {}

  onAddPost(newPost: { title: string; body: string }) {
    this.postsStateService.createPost(newPost).subscribe();
  }

  onDeletePost(post: Post) {
    this.postsStateService.deletePost(post).subscribe();
  }

  ngOnDestroy() {
    this.sub?.unsubscribe();
    this.postsStateService.resetPosts();
  }
}
