import { Component, OnDestroy, OnInit } from '@angular/core';
import { EMPTY, Subscription, switchMap } from 'rxjs';
import { Post } from '../../models';

// Services
import { LoginService } from 'src/app/core/services/login.service';
import { PostsStateService } from '../../services';

@Component({
  selector: 'app-my-posts-page',
  templateUrl: './my-posts-page.component.html',
  styleUrls: ['./my-posts-page.component.scss'],
})
export class MyPostsPageComponent implements OnInit, OnDestroy {
  sub!: Subscription;
  myPosts$ = this.postsStateService.posts$;
  isLoading$ = this.postsStateService.isLoading$;
  error$ = this.postsStateService.error$;
  constructor(private postsStateService: PostsStateService, private loginService: LoginService) {}

  ngOnInit(): void {
    // GET My POSTS FROM SERVER
    this.sub = this.loginService.user$.pipe(
      switchMap((user) => {
        return user ? this.postsStateService.setMyPosts(user.id) : EMPTY;
      })
    ).subscribe();

  }

  onDeletePost(post: Post) {
    this.postsStateService.deletePost(post).subscribe();
  }

  ngOnDestroy() {
    this.postsStateService.resetPosts()
    this.sub?.unsubscribe()
  }
}
