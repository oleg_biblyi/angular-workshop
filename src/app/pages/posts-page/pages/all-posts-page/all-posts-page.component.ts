import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from '../../models';
import { PostsStateService } from '../../services';

@Component({
  selector: 'app-all-posts-page',
  templateUrl: './all-posts-page.component.html',
  styleUrls: ['./all-posts-page.component.scss'],
})
export class AllPostsPageComponent implements OnInit, OnDestroy {
  sub!: Subscription;
  allPosts$ = this.postsStateService.posts$;
  isLoading$ = this.postsStateService.isLoading$;
  error$ = this.postsStateService.error$;

  constructor(private postsStateService: PostsStateService) {}

  ngOnInit(): void {
    // GET ALL POSTS FROM SERVER
    this.postsStateService.setAllPosts().subscribe();
  }

  onDeletePost(post: Post) {
    this.postsStateService.deletePost(post).subscribe();
  }

  ngOnDestroy() {
    this.postsStateService.resetPosts();
    this.sub?.unsubscribe();
  }
}
