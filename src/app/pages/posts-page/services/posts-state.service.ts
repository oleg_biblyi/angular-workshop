import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, EMPTY, tap } from 'rxjs';
import { Post } from '../models';
import { PostsApiService } from './posts-api.service';

@Injectable({
  providedIn: 'root',
})
export class PostsStateService {
  private _posts$ = new BehaviorSubject<Post[]>([]);
  posts$ = this._posts$.asObservable();

  private _isLoading$ = new BehaviorSubject<boolean>(false);
  isLoading$ = this._isLoading$.asObservable();

  private _error$ = new BehaviorSubject<string>('');
  error$ = this._error$.asObservable();

  constructor(private postsApiService: PostsApiService) {}

  setAllPosts() {
    this._isLoading$.next(true);

    return this.postsApiService.getAllPosts().pipe(
      tap((data) => {
        this._isLoading$.next(false);
        this._posts$.next(data);
      }),
      catchError(() => {
        const message = "Error, Couldn't get posts";
        return this.handleError(message);
      })
    );
  }

  setMyPosts(userId: string | number) {
    this._isLoading$.next(true);

    return this.postsApiService.getMyPosts(userId).pipe(
      tap((data) => {
        this._isLoading$.next(false);
        this._posts$.next(data);
      }),
      catchError(() => {
        const message = "Error, Couldn't get my posts";
        return this.handleError(message);
      })
    );
  }

  deletePost(post: Post) {
    this._isLoading$.next(true);
    return this.postsApiService.deletePost(post).pipe(
      tap(() => {
        this._isLoading$.next(false);
        const posts = this._posts$.value.filter((item) => item.id != post.id);
        this._posts$.next(posts);
      }),
      catchError(() => {
        const message = "Error, Couldn't Delete";
        return this.handleError(message);
      })
    );
  }

  createPost({ title, body }: { title: string; body: string }) {
    this._isLoading$.next(true);

    const userId = 1;
    const payload = {
      title,
      body,
      userId,
    };

    return this.postsApiService.createPost(payload).pipe(
      tap((post) => {
        this._isLoading$.next(false);
        const posts = [...this._posts$.value, post];
        this._posts$.next(posts);
      }),
      catchError(() => {
        const message = "Error, Couldn't Delete";
        return this.handleError(message);
      })
    );
  }

  resetPosts() {
    this._posts$.next([]);
    this._isLoading$.next(false);
    this._error$.next('');
  }

  searchPost(value: string): void {
    // this.updatedPosts = this.posts.filter((item) => item.title.includes(value));
  }

  private handleError(message: string) {
    this._isLoading$.next(false);
    this._error$.next(message);
    return EMPTY;
  }
}
