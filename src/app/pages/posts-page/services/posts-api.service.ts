import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from '../models';

@Injectable({
  providedIn: 'root',
})
export class PostsApiService {
  ROOT_URL = 'https://jsonplaceholder.typicode.com/';

  constructor(private http: HttpClient) {}

  getAllPosts() {
    const link = this.ROOT_URL + 'posts';
    return this.http.get<Post[]>(link);
  }

  getMyPosts(userId: string | number) {
    const link = `${this.ROOT_URL}posts?userId=${userId}`;
    return this.http.get<Post[]>(link);
  }

  deletePost(post: Post) {
    const link = this.ROOT_URL + 'posts/' + post.id;
    return this.http.delete<Post>(link);
  }

  createPost(payload: { title: string; body: string; userId: string | number }) {
    const link = this.ROOT_URL + 'posts';
    return this.http.post<Post>(link, payload);
  }
}
